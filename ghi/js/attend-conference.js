window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference'); //dropdown for confr.
    const url = 'http://localhost:8000/api/conferences/'; //fetching to the URL
    const response = await fetch(url); //actual fetching to "GET"
    if (response.ok) {
      const data = await response.json(); //converting json to Javascript

      for (let conference of data.conferences) {
        const option = document.createElement('option'); //creating option element that will appear in dropdown
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option); //adding option tag in the select tag
      }
        const loadingTag = document.getElementById('loading-conference-spinner')
        loadingTag.classList.add('d-none');
        }

    const formTag = document.getElementById('create-attendee-form'); // 여기서 부터 밑에 까지는 processing form
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));

        const locationUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            console.log(newAttendee);
        }
        const successTag = document.getElementById('success-message')
            successTag.classList.remove('d-none')
    });
});
