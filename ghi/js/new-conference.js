window.addEventListener('DOMContentLoaded', async () => { // putting shit on form
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('location');
        // For each state in the states property of the data
        for (let location of data.locations) {
            // Create an 'option' element
            const optionElement = document.createElement('option');
            // Set the '.value' property of the option element to the state's abbreviation
            optionElement.value = location.id;
            // Set the '.innerHTML' property of the option element to the state's name
            optionElement.innerHTML = location.name;
            // Append the option element as a child of the select tag
            selectTag.appendChild(optionElement);
        }
    }
    const formTag = document.getElementById('create-conference-form'); // 여기서 부터 밑에 까지는 processing form
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
