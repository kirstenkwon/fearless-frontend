function createCard(title, location, description, pictureUrl, starts, ends) {
  return `
    <div class="card mb-3 shadow">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
          ${new Date(starts).toLocaleDateString()} -
          ${new Date(ends).toLocaleDateString()}
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let col = 0;
        for (let conference of data.conferences) {
          // Assigns the URL for the conference to detailURL
          const detailUrl = `http://localhost:8000${conference.href}`;
          // Fetches the url resource from the server
          // await - waits for the response to fulfill (needs to be an async function)
          const detailResponse = await fetch(detailUrl);
          // Triggers if the response.ok -- true
          if (detailResponse.ok) {

              // Returns and assigns a promise that resolves to a JS object
              const details = await detailResponse.json();
              console.log(details)
              const title = details.conference.name;
              const location = details.conference.location.name;
              const description = details.conference.description;
              const start = details.conference.starts;
              const end = details.conference.ends;
              const pictureUrl = details.conference.location.picture_url;
              const html = createCard(title, location, description, pictureUrl, start, end);
              console.log(html)
              const conferenceTag = document.getElementById(`col-${col}`) // or querySelector(`#col....)
              col++
              if (col > 2) {
                  col = 0;
              }
              conferenceTag.innerHTML += html
              console.log(html)
          }
    }
}
} catch(e) {
  console.error(e);
      // Figure out what to do if an error is raised
    }
  });
