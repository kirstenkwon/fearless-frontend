import React, {useEffect, useState} from 'react';

function LocationForm() {
    //Set the useState hook to store "name" in the component's state, with a default initial value of an empty string
    const [states, setStates] = useState([]); //list of states for dropdown
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState(''); //creating folder in cabinet

    const fetchData = async () => { // this is new
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states);

        //     // Get the select tag element by its id 'state'
        //     const selectTag = document.getElementById('state');
        //     // For each state in the states property of the data
        //     for (let state of data.states) {
        //         // Create an 'option' element
        //         const option = document.createElement('option');
        //         // Set the '.value' property of the option element to the state's abbreviation
        //         option.value = state.abbreviation;
        //         // Set the '.innerHTML' property of the option element to the state's name
        //         option.innerHTML = state.name + " ("+state.abbreviation+")";
        //         // Append the option element as a child of the select tag
        //         selectTag.appendChild(PushSubscriptionOptions);
        //     }
        // }
    // }
        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();  // prevent page from reloading
        const data = {}; // create an empty JSON object

        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post", //post request
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setState('');
        }
        }

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value); //putting info in folder in the cabinet
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" value={roomCount} id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" value={city} name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleStateChange} required name="state" id="state" className="form-select" value={state}>
                    <option value="">Choose a state</option>
                    {states.map(state => { //processes wahtever state you select
                        return (
                            <option key={state.abbreviation} value={state.abbreviation}>
                                {state.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default LocationForm;
