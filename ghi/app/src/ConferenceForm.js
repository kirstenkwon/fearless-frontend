import React, {useEffect, useState} from 'react';

function ConferenceForm() {
        //Set the useState hook to store "name" in the component's state, with a default initial value of an empty string
    const [locations, setLocations] = useState([]); //list of locations for dropdown
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState(''); //creating folder in cabinet
    const [maxAttendees, setMaxAttendees] = useState(''); //creating folder in cabinet
    const [location, setLocation] = useState('');

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value); //putting info in folder in the cabinet
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value); //putting info in folder in the cabinet
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value); //putting info in folder in the cabinet
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }


    const fetchData = async () => { // this is new
        const url = 'http://localhost:8000/api/locations/'; // here to line 27 from new-location.js
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" value={starts} id="starts" className="form-control" />
                <label htmlFor="room_count">Starts</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleEndsChange} placeholder="Ends" required type="date" value={ends} name="ends" id="ends" className="form-control" />
                <label htmlFor="city">Ends</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleDescriptionChange} placeholder="Description" required type="description" value={description} name="description" id="description" className="form-control" />
                <label htmlFor="city">Description</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="text" value={maxPresentations} name="MaxPresentations" id="MaxPresentations" className="form-control" />
                <label htmlFor="city">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="text" value={maxAttendees} name="MaxAttendees" id="MaxAttendees" className="form-control" />
                <label htmlFor="city">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                        <option value="">Choose a Location</option>
                        {locations.map(location => { //processes wahtever state you select
                            return (
                                <option key={location.id} value={location.id}>
                                    {location.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}


export default ConferenceForm;
